#!/bin/env fish

set FILTER_VERSION 0.5.1

docker build --no-cache -t looztra/guestbook-filter:$FILTER_VERSION-all-envs-in-one -f Dockerfile.all-envs-in-one .
docker push looztra/guestbook-filter:$FILTER_VERSION-all-envs-in-one