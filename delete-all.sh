#!/bin/env fish

set app_version 2.5.0

for app in filter frontend gateway service-server
  for env in dev-local acc-1 prod
    echo "Removing image for env [$env] app app [$app] in version [$app_version]"
    echo "====================================>"
    echo docker rmi looztra/env-guestbook:$app--$env--$app_version
    docker rmi looztra/env-guestbook:$app--$env--$app_version
    echo "<===================================="
  end
end