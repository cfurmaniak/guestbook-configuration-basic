#!/bin/env fish

set app_version 1.0.2
set build_opts

for app in filter frontend gateway service-server
  for env in dev-local acc-1 prod
    echo "Building env [$env] for app [$app]"
    echo "====================================>"
    echo docker build $build_opts -t looztra/env-guestbook:$app--$env--$app_version $app/$env/
    docker build $build_opts -t looztra/env-guestbook:$app--$env--$app_version $app/$env/
    docker push looztra/env-guestbook:$app--$env--$app_version
    echo "<===================================="
  end
end
